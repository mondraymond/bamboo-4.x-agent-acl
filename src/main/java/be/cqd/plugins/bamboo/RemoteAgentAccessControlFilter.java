package be.cqd.plugins.bamboo;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.BuildAgentRequirementFilter;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementSet;
import com.atlassian.bamboo.v2.build.trigger.ManualBuildTriggerReason;
import com.atlassian.user.User;

import java.util.Collection;

/**
 * RemoteAgentAccessControlFilter solves the problem that there are no ACLs for Agent access within Bamboo.
 * <p/>
 * By default this method is provided with a list of agents that match its specified requirements.  These requirements may overlap with
 * the capabilities of many agents so that bamboo can load balance and run as many builds on as many agents as possible. Whilst this has
 * some advantages, it also means that there is the possibility of offering a production agent for a development (or other).
 * <p/>
 * The solution is to have Bamboo admins define two capabilities per agent:
 * <p/>
 * AGENT_TARGET_ENVIRONMENT
 * <p/>
 * - DEVELOPMENT
 * <p/>
 * - TEST
 * <p/>
 * - PRODUCTION
 * <p/>
 * - etc...
 * <p/>
 * [ Matched as a single Fixed String ]
 * <p/>
 * AGENT_ALLOWED_GROUPS
 * <p/>
 * - cde-ea-developers, cde-operations (standard group names)
 * <p/>
 * - cde-admin.* (group name expanded with regular expression)
 * <p/>
 * - .* (regular expression matching any group)
 * <p/>
 * [ This is a matched as comma delimited expression.  Each token supports regular expression matching per group definition. ]
 * <p/>
 * This filter will perform the ACL matching
 */
public class RemoteAgentAccessControlFilter implements BuildAgentRequirementFilter {

    private PlanKeyFinder planKeyFinder;

    RemoteAgentAccessControlFilter() {
        planKeyFinder = new PlanKeyHelper();
    }

    // Enable mocking of the PlanKeyFinder
    RemoteAgentAccessControlFilter(PlanKeyFinder planKeyFinder) {
        this.planKeyFinder = planKeyFinder;
    }

    private BuildLogger buildLogger;
    private BuildLoggerManager buildLoggerManager;
    private BambooUserManager bambooUserManager;

    public Collection<BuildAgent> filter(final BuildContext buildContext, final Collection<BuildAgent> buildAgents, final RequirementSet requirementSet) {

        buildLogger = buildLoggerManager.getBuildLogger(planKeyFinder.getPlanKey(buildContext));

        AccessFilter accessFilter = new AccessFilter();

        // If all offered build agents are local agents, we can let them pass
        Collection<BuildAgent> localAgents = accessFilter.findLocalAgents(buildAgents);
        if (localAgents.size() == buildAgents.size())
            return buildAgents;

        // We only need to perform ACL check on remote agents
        Collection<BuildAgent> remoteAgents = accessFilter.findRemoteAgents(buildAgents);

        // First ensure that the system to manage capabilities is properly configured by the Bamboo administrators
        Collection<BuildAgent> validatedAgents = accessFilter.findProperlyConfiguredAgents(remoteAgents);

        // Cancel the build if there are no validated remote agents and no local agents.  Otherwise return local agents
        if (validatedAgents.isEmpty()) {
            if (localAgents.isEmpty())
                cancelTheBuild("There are no agents with the minimum capability definitions of " + AccessFilter.AGENT_ALLOWED_GROUPS + " and " + AccessFilter.AGENT_TARGET_ENVIRONMENT);
            else
                return localAgents;
        }

        // We ensure that remote agents are only deployed manually, since Bamboo cannot provide user data for automatically triggered builds
        if (buildContext.getTriggerReason() instanceof ManualBuildTriggerReason) {
            User user = bambooUserManager.getUser(((ManualBuildTriggerReason) buildContext.getTriggerReason()).getUserName());

            Collection<BuildAgent> allowedAgents = accessFilter.findAgentsAllowedForThisUser(bambooUserManager.getGroupNamesAsList(user), validatedAgents);

            // Cancel the build if there are no authorized remote agents and no local agents.  Otherwise return local agents
            if (allowedAgents.isEmpty())
                if (localAgents.isEmpty())
                    cancelTheBuild("There are no authorized agents for " + user.getName());
                else
                    return localAgents;

            // The list of allowed agents
            return allowedAgents;
        }

        // Don't return any remote agents and cancel the build if there are no local builds
        if (localAgents.isEmpty())
            cancelTheBuild("There are no agents that match this build's requirements");

        // This should never be executed since local agents will already have been returned.
        return localAgents;
    }


    private void cancelTheBuild(final String reason) {
        buildLogger.addErrorLogEntry("Cancelling the build: " + reason);
        throw new IllegalStateException(reason);
    }


    public void setBambooUserManager(BambooUserManager bambooUserManager) {
        this.bambooUserManager = bambooUserManager;
    }

    public void setBuildLoggerManager(BuildLoggerManager buildLoggerManager) {
        this.buildLoggerManager = buildLoggerManager;
    }

}
