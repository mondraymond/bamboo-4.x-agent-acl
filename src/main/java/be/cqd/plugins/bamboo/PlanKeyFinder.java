package be.cqd.plugins.bamboo;

import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.v2.build.BuildContext;

/**
 * Created by IntelliJ IDEA.
 * User: ray
 * Date: 16/03/11
 * Time: 21:01
 * To change this template use File | Settings | File Templates.
 */
public interface PlanKeyFinder {
    public PlanKey getPlanKey(BuildContext buildContext);
}
