package be.cqd.plugins.bamboo;

import com.atlassian.bamboo.buildqueue.properties.CapabilityProperties;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.LocalBuildAgent;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;

import java.util.*;

/**
 * User: ray
 * Date: 14/03/11
 * Time: 08:19
 */
public class AccessFilter {

    public static final String AGENT_TARGET_ENVIRONMENT = "AGENT_TARGET_ENVIRONMENT";
    public static final String AGENT_ALLOWED_GROUPS = "AGENT_ALLOWED_GROUPS";

    public Collection<BuildAgent> findRemoteAgents(final Collection<BuildAgent> buildAgents) {
        final Collection<BuildAgent> remoteAgents = new ArrayList<BuildAgent>();

        for (BuildAgent buildAgent : buildAgents)
            if (!(buildAgent instanceof LocalBuildAgent))
                remoteAgents.add(buildAgent);

        return remoteAgents;
    }

    public Collection<BuildAgent> findLocalAgents(final Collection<BuildAgent> buildAgents) {
        final Collection<BuildAgent> localAgents = new ArrayList<BuildAgent>();

        for (BuildAgent buildAgent : buildAgents)
            if (buildAgent instanceof LocalBuildAgent)
                localAgents.add(buildAgent);

        return localAgents;
    }

    // User groups can include regular expressions
    public Collection<BuildAgent> findAgentsAllowedForThisUser(final List<String> userGroups, final Collection<BuildAgent> agents) {
        final Set<BuildAgent> allowedAgents = new HashSet<BuildAgent>();

        for (BuildAgent agent : agents) {
            String agentGroups = getCapabilityValue(((CapabilityProperties) agent.getDefinition()).getCapabilitySet(), AGENT_ALLOWED_GROUPS);
            StringTokenizer agentGroupTokens = new StringTokenizer(agentGroups, ",");

            while (agentGroupTokens.hasMoreElements()) {
                String agentGroup = agentGroupTokens.nextToken().trim();
                for (String userGroup : userGroups)
                    if (userGroup.matches(agentGroup))
                        allowedAgents.add(agent);
            }
        }

        return allowedAgents;
    }

    public Collection<BuildAgent> findProperlyConfiguredAgents(final Collection<BuildAgent> agents) {
        final Collection<BuildAgent> validatedAgents = new ArrayList<BuildAgent>();

        for (BuildAgent agent : agents)
            if (validateMandatoryAgentCapabilityDefinitions((CapabilityProperties) agent.getDefinition()))
                validatedAgents.add(agent);

        return validatedAgents;
    }

    private boolean validateMandatoryAgentCapabilityDefinitions(final CapabilityProperties agentDefinition) {
        final boolean targetEnvironmentDefined = getCapabilityValue(agentDefinition.getCapabilitySet(), AGENT_TARGET_ENVIRONMENT) == null ? false : true;
        final boolean allowedGroupsDefined = getCapabilityValue(agentDefinition.getCapabilitySet(), AGENT_ALLOWED_GROUPS) == null ? false : true;

        return targetEnvironmentDefined && allowedGroupsDefined;
    }

    private String getCapabilityValue(final ReadOnlyCapabilitySet capabilitySet, final String key) {
        for (Capability capability : capabilitySet.getCapabilities())
            if (capability.getKey().equalsIgnoreCase(key))
                return capability.getValue();

        return null;
    }

    /*
        deprecated (before publishing !)

    public static final String PRODUCTION_ENVIRONMENT = "PRODUCTION";

    public Collection<BuildAgent> findProductionAgents(Collection<BuildAgent> agents) {
        Collection<BuildAgent> productionAgents = new ArrayList<BuildAgent>();

        for (BuildAgent agent : agents)
            if (getCapabilityValue(((CapabilityProperties) agent.getDefinition()).getCapabilitySet(), RemoteAgentAccessControlFilter.PRODUCTION_ENVIRONMENT) != null)
                productionAgents.add(agent);

        return productionAgents;
    }


    private boolean checkIfProductionIsTheTargetEnvironment(RequirementSet requirementSet) {
        return getRequirementValue(requirementSet, RemoteAgentAccessControlFilter.PRODUCTION_ENVIRONMENT) == null ? false : true;

    private String getRequirementValue(final RequirementSet requirements, String key) {
        for (Requirement requirement : requirements.getRequirements())
            if (requirement.getKey().equalsIgnoreCase(key))
                return requirement.getMatchValue();

        return null;
    }


    }

     */

}
