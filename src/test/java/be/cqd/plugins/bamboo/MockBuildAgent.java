package be.cqd.plugins.bamboo;

import com.atlassian.bamboo.buildqueue.PipelineDefinition;
import com.atlassian.bamboo.buildqueue.properties.CapabilityProperties;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * User: ray
 * Date: 16/03/11
 * Time: 17:30
 */
public class MockBuildAgent {

    private String targetEnvironmentKey = "None";
    private String targetEnvironmentValue = "None";
    private String allowedGroupsKey = "None";
    private String allowedGroupsValue = "None";

    // Default configured mock
    public BuildAgent newConfiguredBuildAgent() {
        setTargetEnvironmentKey(AccessFilter.AGENT_TARGET_ENVIRONMENT);
        setTargetEnvironmentValue("PRODUCTION");

        setAllowedGroupsKey(AccessFilter.AGENT_ALLOWED_GROUPS);
        setAllowedGroupsValue("Administrators, Operators");
        return newBuildAgent();
    }

    // Can get one of these and set the properties as you wish...
    public BuildAgent newBuildAgent() {
        Capability AGENT_TARGET_ENVIRONMENT = mock(Capability.class);
        when(AGENT_TARGET_ENVIRONMENT.getKey()).thenReturn(targetEnvironmentKey);
        when(AGENT_TARGET_ENVIRONMENT.getValue()).thenReturn(targetEnvironmentValue);

        Capability AGENT_ALLOWED_GROUPS = mock(Capability.class);
        when(AGENT_ALLOWED_GROUPS.getKey()).thenReturn(allowedGroupsKey);
        when(AGENT_ALLOWED_GROUPS.getValue()).thenReturn(allowedGroupsValue);

        Set<Capability> capabilities = new HashSet<Capability>();
        capabilities.add(AGENT_TARGET_ENVIRONMENT);
        capabilities.add(AGENT_ALLOWED_GROUPS);

        CapabilitySet capabilitySet = mock(CapabilitySet.class);
        when(capabilitySet.getCapabilities()).thenReturn(capabilities);

        CapabilityProperties capabilityProperties = mock(FrigToDealWithBizarreAtlassianCasting.class);
        when(capabilityProperties.getCapabilitySet()).thenReturn(capabilitySet);

        BuildAgent buildAgent = mock(BuildAgent.class);
        when(buildAgent.getDefinition()).thenReturn((PipelineDefinition) capabilityProperties);   // Should not need to be cast
        return buildAgent;
    }

    public void setTargetEnvironmentKey(String targetEnvironmentKey) {
        this.targetEnvironmentKey = targetEnvironmentKey;
    }

    public void setTargetEnvironmentValue(String targetEnvironmentValue) {
        this.targetEnvironmentValue = targetEnvironmentValue;
    }

    public void setAllowedGroupsKey(String allowedGroupsKey) {
        this.allowedGroupsKey = allowedGroupsKey;
    }

    public void setAllowedGroupsValue(String allowedGroupsValue) {
        this.allowedGroupsValue = allowedGroupsValue;
    }

    private interface FrigToDealWithBizarreAtlassianCasting extends CapabilityProperties, PipelineDefinition {
    }

}
