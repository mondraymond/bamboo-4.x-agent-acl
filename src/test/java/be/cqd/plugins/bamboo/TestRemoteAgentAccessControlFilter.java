package be.cqd.plugins.bamboo;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.LocalBuildAgent;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementSet;
import com.atlassian.bamboo.v2.build.trigger.ManualBuildTriggerReason;
import com.atlassian.user.User;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * User: ray
 * Date: 16/03/11
 * Time: 18:58
 */
public class TestRemoteAgentAccessControlFilter {

    @Test
    public void testMatchingAgents() {
        RemoteAgentAccessControlFilter remoteAgentAccessControlFilter = new RemoteAgentAccessControlFilter(new MockPlanKeyHelper());

        BambooUserManager bambooUserManager = mock(BambooUserManager.class);
        when(bambooUserManager.getGroupNamesAsList(Mockito.<User>any())).thenReturn(Arrays.asList("Administrators"));

        remoteAgentAccessControlFilter.setBambooUserManager(bambooUserManager);

        BuildLoggerManager buildLoggerManager = mock(BuildLoggerManager.class);
        BuildLogger buildLogger = mock(BuildLogger.class);
        when(buildLoggerManager.getBuildLogger(Mockito.<PlanKey>any())).thenReturn(buildLogger);

        remoteAgentAccessControlFilter.setBuildLoggerManager(buildLoggerManager);

        MockBuildAgent mockBuildAgent = new MockBuildAgent();
        mockBuildAgent.setTargetEnvironmentKey(AccessFilter.AGENT_TARGET_ENVIRONMENT);
        mockBuildAgent.setTargetEnvironmentValue("TEST1");
        mockBuildAgent.setAllowedGroupsKey(AccessFilter.AGENT_ALLOWED_GROUPS);
        mockBuildAgent.setAllowedGroupsValue("Administrators");

        BuildAgent agent = mockBuildAgent.newBuildAgent();

        BuildContext buildContext = mock(BuildContext.class);
        ManualBuildTriggerReason manualBuildTriggerReason = mock(ManualBuildTriggerReason.class);
        when(buildContext.getTriggerReason()).thenReturn(manualBuildTriggerReason);

        Assert.assertTrue(remoteAgentAccessControlFilter.filter(buildContext, Arrays.asList(agent), mock(RequirementSet.class)).size() == 1);
    }

    @Test(expected = IllegalStateException.class)
    public void testNonMatchingNotPermittedAgents() {
        RemoteAgentAccessControlFilter remoteAgentAccessControlFilter = new RemoteAgentAccessControlFilter(new MockPlanKeyHelper());

        BambooUserManager bambooUserManager = mock(BambooUserManager.class);
        when(bambooUserManager.getGroupNamesAsList(Mockito.<User>any())).thenReturn(Arrays.asList("Administrators"));

        remoteAgentAccessControlFilter.setBambooUserManager(bambooUserManager);

        BuildLoggerManager buildLoggerManager = mock(BuildLoggerManager.class);
        BuildLogger buildLogger = mock(BuildLogger.class);
        when(buildLoggerManager.getBuildLogger(Mockito.<PlanKey>any())).thenReturn(buildLogger);

        remoteAgentAccessControlFilter.setBuildLoggerManager(buildLoggerManager);

        MockBuildAgent mockBuildAgent = new MockBuildAgent();
        mockBuildAgent.setTargetEnvironmentKey(AccessFilter.AGENT_TARGET_ENVIRONMENT);
        mockBuildAgent.setTargetEnvironmentValue("TEST1");
        mockBuildAgent.setAllowedGroupsKey(AccessFilter.AGENT_ALLOWED_GROUPS);
        mockBuildAgent.setAllowedGroupsValue("Administrators");

        BuildAgent agent = new MockBuildAgent().newBuildAgent();

        BuildContext buildContext = mock(BuildContext.class);
        ManualBuildTriggerReason manualBuildTriggerReason = mock(ManualBuildTriggerReason.class);
        when(buildContext.getTriggerReason()).thenReturn(manualBuildTriggerReason);

        remoteAgentAccessControlFilter.filter(buildContext, Arrays.asList(agent), mock(RequirementSet.class));
    }

    @Test
    public void testNonMatchingLocalAndNotPermittedAgents() {
        RemoteAgentAccessControlFilter remoteAgentAccessControlFilter = new RemoteAgentAccessControlFilter(new MockPlanKeyHelper());

        BambooUserManager bambooUserManager = mock(BambooUserManager.class);
        when(bambooUserManager.getGroupNamesAsList(Mockito.<User>any())).thenReturn(Arrays.asList("Administrators"));

        remoteAgentAccessControlFilter.setBambooUserManager(bambooUserManager);

        BuildLoggerManager buildLoggerManager = mock(BuildLoggerManager.class);
        BuildLogger buildLogger = mock(BuildLogger.class);
        when(buildLoggerManager.getBuildLogger(Mockito.<PlanKey>any())).thenReturn(buildLogger);

        remoteAgentAccessControlFilter.setBuildLoggerManager(buildLoggerManager);

        MockBuildAgent mockBuildAgent = new MockBuildAgent();
        mockBuildAgent.setTargetEnvironmentKey(AccessFilter.AGENT_TARGET_ENVIRONMENT);
        mockBuildAgent.setTargetEnvironmentValue("TEST1");
        mockBuildAgent.setAllowedGroupsKey(AccessFilter.AGENT_ALLOWED_GROUPS);
        mockBuildAgent.setAllowedGroupsValue("None");

        BuildAgent agent = new MockBuildAgent().newBuildAgent();

        Collection<BuildAgent> buildAgents = new ArrayList<BuildAgent>();
        buildAgents.add(agent);
        buildAgents.add(mock(LocalBuildAgent.class));

        BuildContext buildContext = mock(BuildContext.class);
        ManualBuildTriggerReason manualBuildTriggerReason = mock(ManualBuildTriggerReason.class);
        when(buildContext.getTriggerReason()).thenReturn(manualBuildTriggerReason);

        Assert.assertTrue(remoteAgentAccessControlFilter.filter(buildContext, buildAgents, mock(RequirementSet.class)).size() == 1);
    }

    @Test
    public void testLocalAgentsOnlyMatchingFilters() {
        RemoteAgentAccessControlFilter remoteAgentAccessControlFilter = new RemoteAgentAccessControlFilter(new MockPlanKeyHelper());

        BuildLoggerManager buildLoggerManager = mock(BuildLoggerManager.class);
        BuildLogger buildLogger = mock(BuildLogger.class);
        when(buildLoggerManager.getBuildLogger(Mockito.<PlanKey>any())).thenReturn(buildLogger);

        remoteAgentAccessControlFilter.setBuildLoggerManager(buildLoggerManager);

        BuildContext buildContext = mock(BuildContext.class);
        ManualBuildTriggerReason manualBuildTriggerReason = mock(ManualBuildTriggerReason.class);
        when(buildContext.getTriggerReason()).thenReturn(manualBuildTriggerReason);

        BuildAgent buildAgent = mock(LocalBuildAgent.class);

        Assert.assertTrue(remoteAgentAccessControlFilter.filter(buildContext, Arrays.asList(buildAgent), mock(RequirementSet.class)).size() == 1);
    }

}
