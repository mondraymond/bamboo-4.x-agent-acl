package be.cqd.plugins.bamboo;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.LocalBuildAgent;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.mockito.Mockito.mock;

/**
 * User: ray
 * Date: 14/03/11
 * Time: 08:05
 */
public class TestAccessFilter {

    @Test
    public void testFindLocalAgentsWithOnlyLocalAgents() {
        BuildAgent buildAgent = mock(LocalBuildAgent.class);
        Assert.assertTrue(new AccessFilter().findLocalAgents(Arrays.asList(buildAgent)).size() == 1);
    }

    @Test
    public void testFindLocalAgentsWithOnlyRemoteAgents() {
        BuildAgent buildAgent = mock(BuildAgent.class);
        Assert.assertTrue(new AccessFilter().findLocalAgents(Arrays.asList(buildAgent)).isEmpty());
    }

    @Test
    public void testFindLocalAgentsWithMixedTypesOfAgents() {
        Collection<BuildAgent> buildAgents = new ArrayList<BuildAgent>();
        buildAgents.add(mock(BuildAgent.class));
        buildAgents.add(mock(LocalBuildAgent.class));

        Assert.assertTrue(new AccessFilter().findLocalAgents(buildAgents).size() == 1);
    }

    @Test
    public void testFindRemoteAgentsWithOnlyLocalAgents() {
        BuildAgent buildAgent = mock(LocalBuildAgent.class);
        Assert.assertTrue(new AccessFilter().findRemoteAgents(Arrays.asList(buildAgent)).isEmpty());
    }

    @Test
    public void testFindRemoteAgentsWithOnlyRemoteAgents() {
        BuildAgent buildAgent = mock(BuildAgent.class);
        Assert.assertTrue(new AccessFilter().findRemoteAgents(Arrays.asList(buildAgent)).size() == 1);
    }

    @Test
    public void testFindRemoteAgentsWithMixedTypesOfAgents() {
        Collection<BuildAgent> buildAgents = new ArrayList<BuildAgent>();
        buildAgents.add(mock(BuildAgent.class));
        buildAgents.add(mock(LocalBuildAgent.class));

        Assert.assertTrue(new AccessFilter().findRemoteAgents(buildAgents).size() == 1);
    }

    @Test
    public void testFindProperlyConfiguredAgentsWithProperlyConfiguredAgents() {
        Assert.assertTrue(new AccessFilter().findProperlyConfiguredAgents(Arrays.asList(getProductionConfiguredMockAgent())).size() == 1);
    }

    @Test
    public void testFindProperlyConfiguredAgentsWithoutProperlyConfiguredAgents() {
        Assert.assertTrue(new AccessFilter().findProperlyConfiguredAgents(Arrays.asList(getNonProductionConfiguredMockAgent())).isEmpty());
    }

    @Test
    public void testfindAgentsAllowedForThisUserWithAnAllowedAgent() {
        MockBuildAgent mockBuildAgent = new MockBuildAgent();
        mockBuildAgent.setTargetEnvironmentKey(AccessFilter.AGENT_TARGET_ENVIRONMENT);
        mockBuildAgent.setTargetEnvironmentValue("TEST1");
        mockBuildAgent.setAllowedGroupsKey(AccessFilter.AGENT_ALLOWED_GROUPS);

        mockBuildAgent.setAllowedGroupsValue("Administrators");
        BuildAgent agent = mockBuildAgent.newBuildAgent();

        Assert.assertTrue(new AccessFilter().findAgentsAllowedForThisUser(Arrays.asList("Administrators"), Arrays.asList(agent)).size() == 1);

        mockBuildAgent.setAllowedGroupsValue("Admin.*, Operators");
        agent = mockBuildAgent.newBuildAgent();

        // Explicitly matches an item in the list
        Assert.assertTrue(new AccessFilter().findAgentsAllowedForThisUser(Arrays.asList("Operators"), Arrays.asList(agent)).size() == 1);

        // Matches via the expression
        Assert.assertTrue(new AccessFilter().findAgentsAllowedForThisUser(Arrays.asList("Administrators"), Arrays.asList(agent)).size() == 1);

        // Does not match from a single value
        Assert.assertTrue(new AccessFilter().findAgentsAllowedForThisUser(Arrays.asList("Development"), Arrays.asList(agent)).isEmpty());

        // Does not match from a list
        Assert.assertTrue(new AccessFilter().findAgentsAllowedForThisUser(Arrays.asList("g1, gr2, g3, g4"), Arrays.asList(agent)).isEmpty());
    }

    @Test
    public void testfindAgentsAllowedForThisUserWithOutAnAllowedAgent() {
        Collection<BuildAgent> agents = Arrays.asList(getProductionConfiguredMockAgent());
        List<String> groupsToWhichThisUserBelongs = Arrays.asList("BadGroup");
        Assert.assertTrue(new AccessFilter().findAgentsAllowedForThisUser(groupsToWhichThisUserBelongs, agents).isEmpty());
    }

    private BuildAgent getProductionConfiguredMockAgent() {
        return new MockBuildAgent().newConfiguredBuildAgent();
    }

    private BuildAgent getNonProductionConfiguredMockAgent() {
        return new MockBuildAgent().newBuildAgent();
    }

}
